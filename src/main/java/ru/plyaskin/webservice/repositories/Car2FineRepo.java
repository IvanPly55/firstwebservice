package ru.plyaskin.webservice.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.plyaskin.webservice.model.CarToFine;

public interface Car2FineRepo extends JpaRepository<CarToFine, Integer> {

}
