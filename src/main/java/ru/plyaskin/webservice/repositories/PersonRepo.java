package ru.plyaskin.webservice.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.plyaskin.webservice.model.Person;

import java.util.Optional;

@Repository
public interface PersonRepo extends JpaRepository<Person, Integer> {

}
