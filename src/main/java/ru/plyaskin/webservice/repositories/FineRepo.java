package ru.plyaskin.webservice.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.plyaskin.webservice.model.Fine;

public interface FineRepo extends JpaRepository<Fine, Integer> {
}
