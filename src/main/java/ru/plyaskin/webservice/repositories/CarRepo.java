package ru.plyaskin.webservice.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.plyaskin.webservice.model.Car;

@Repository
public interface CarRepo extends JpaRepository<Car, Integer> {
    Car findCarByLicensePlate(String licensePlate);
}
