package ru.plyaskin.webservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.plyaskin.webservice.dto.CarInfoDtoRequest;
import ru.plyaskin.webservice.model.Car;
import ru.plyaskin.webservice.model.Person;
import ru.plyaskin.webservice.repositories.CarRepo;
import ru.plyaskin.webservice.repositories.PersonRepo;
import ru.plyaskin.webservice.service.FakeInfoCreationServiceImpl;
import java.util.Date;
import java.util.List;

@RestController
public class ServiceController {

//    @Autowired
//    private RegistrationService registrationService;

    @Autowired
    private PersonRepo personRepo;
    @Autowired
    private CarRepo carRepo;

    @Autowired
    private FakeInfoCreationServiceImpl fakeInfoCreationService;

    public ServiceController(){
    }

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public List<Person> greeting() {
        return personRepo.findAll();
    }

    @RequestMapping(value = "/car", method = RequestMethod.GET)
    public List<Car> greetings() {
        return carRepo.findAll();
    }

    @RequestMapping(value = "/dall", method = RequestMethod.GET)
    public void deleteAll() {
        personRepo.deleteAll();
    }

    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public String addNewPerson() {
        Person newPerson = new Person();
        newPerson.setFirstName("Ivan");
        newPerson.setLastName("Plyaskin");
        personRepo.save(newPerson);
        return "Saved";
    }

    @RequestMapping(value = "/fake", method = RequestMethod.GET)
    public String generateFakeInfo() {
            fakeInfoCreationService.generateFakeInfo();
            return "Fake info saved";
    }

    @PostMapping(value = "/search", consumes = "application/json", produces = "application/json")
    public Car findPerson(@RequestBody CarInfoDtoRequest request) {
           return carRepo.findCarByLicensePlate(request.getLicensePlate());
    }

//    @RequestMapping(value = "/test", method = RequestMethod.GET)
//    public String test() {
//        return fakeInfoCreationService.test();
//    }
//
//    @PostMapping(value = "/search", consumes = "application/json", produces = "application/json")
//    public Person findPerson(@RequestBody PersonDtoRequest request) {
//        Optional<Person> person = personRepo.findById(request.getPersonId());
//        //return person.isPresent() ? person.get() ;
//        return null;
//    }


    @RequestMapping(value = "/date", method = RequestMethod.GET)
    public Date getDate() {
        return new Date();
    }

}
