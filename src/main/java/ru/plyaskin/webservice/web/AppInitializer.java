package ru.plyaskin.webservice.web;

import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.context.support.GenericWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;

public class AppInitializer implements WebApplicationInitializer {
    @Override
    public void onStartup(ServletContext servletContext) throws ServletException {

        // Load Spring web application configuration
        AnnotationConfigWebApplicationContext root = new AnnotationConfigWebApplicationContext();

        root.scan("ru.plyaskin.webservice");
        servletContext.addListener(new ContextLoaderListener(root));

        // Create and register the DispatcherServlet
        DispatcherServlet servlet = new DispatcherServlet(new GenericWebApplicationContext());
        ServletRegistration.Dynamic appServlet = servletContext.addServlet("app", servlet);
        appServlet.setLoadOnStartup(1);
        appServlet.addMapping("/");
    }
}
