package ru.plyaskin.webservice.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "CAR_INFO")
public class Car {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "CAR_ID")
    private Integer carId;

    @Column(name = "LICENSE_PLATE")
    private String licensePlate;

    @Column(name = "CAR_MODEL")
    private String carModel;

    @Column(name = "MANUFACTURE_YEAR")
    private String yearOfCarManufacture;

    @ManyToOne (targetEntity = Person.class, fetch=FetchType.EAGER, cascade=CascadeType.ALL)
    @JoinColumn (name = "CAR_OWNER")
    private Person carOwner;

    @OneToMany(targetEntity = Fine.class, fetch = FetchType.LAZY, mappedBy = "carOwner", cascade = CascadeType.ALL)
    private List<Fine> fines = new ArrayList<>();

    public Car() {
    }

    public Integer getCarId() {
        return carId;
    }

    public String getLicensePlate() {
        return licensePlate;
    }

    public void setLicensePlate(String licensePlate) {
        this.licensePlate = licensePlate;
    }

    public String getCarModel() {
        return carModel;
    }

    public void setCarModel(String carModel) {
        this.carModel = carModel;
    }

    public String getYearOfCarManufacture() {
        return yearOfCarManufacture;
    }

    public void setYearOfCarManufacture(String yearOfCarManufacture) {
        this.yearOfCarManufacture = yearOfCarManufacture;
    }

    public Person getCarOwner() {
        return carOwner;
    }

    public void setCarOwner(Person person) {
        this.carOwner = person;
    }

    @Override
    public String toString() {
        return "Car{" +
                "carId=" + carId +
                ", licensePlate='" + licensePlate + '\'' +
                ", carModel='" + carModel + '\'' +
                ", yearOfCarManufacture='" + yearOfCarManufacture.toString() + '\'' +
                ", person=" + carOwner +
                '}';
    }
}
