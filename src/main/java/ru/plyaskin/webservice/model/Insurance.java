package ru.plyaskin.webservice.model;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "INSURANCE_INFO")
public class Insurance {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "INSURANCE_ID")
    private Integer insuranceId;

    @Column(name = "INSURANCE_NUMBER")
    private Integer insuranceNumber;

    @Column(name = "START_DATE")
    private Date startDate;

    @Column(name = "EXPIRATION_DATE")
    private Date expirationDate;


    @OneToOne(fetch=FetchType.LAZY, cascade=CascadeType.ALL)
    @JoinColumn (name = "CAR_ID")
    private Car car;
}
