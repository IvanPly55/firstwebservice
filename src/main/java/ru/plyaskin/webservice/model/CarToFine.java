package ru.plyaskin.webservice.model;

import javax.persistence.*;

@Entity
@Table(name = "CAR2FINE")
public class CarToFine {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "GID")
    private Integer id;

    @OneToOne(targetEntity = Car.class, fetch = FetchType.EAGER, cascade=CascadeType.ALL)
    @JoinColumn(name = "CAR_ID")
    private Car carId;

    @OneToOne(targetEntity = Fine.class, fetch = FetchType.EAGER, cascade=CascadeType.ALL)
    @JoinColumn(name = "FINE_ID")
    private Fine fineId;

    public CarToFine() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Car getCarId() {
        return carId;
    }

    public void setCarId(Car carId) {
        this.carId = carId;
    }

    public Fine getFineId() {
        return fineId;
    }

    public void setFineId(Fine fineId) {
        this.fineId = fineId;
    }

    @Override
    public String toString() {
        return "CarToFine{" +
                "id=" + id +
                ", carId=" + carId +
                ", fineId=" + fineId +
                '}';
    }
}
