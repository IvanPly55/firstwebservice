package ru.plyaskin.webservice.model;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "FINE_INFO")
public class Fine {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "FINE_ID")
    private Integer fineId;

    @Column(name = "ACTIVE")
    private String isActive;

    @Column(name = "START_DATE")
    private String startDate;

    @Column(name = "EXPIRATION_DATE")
    private String expirationDate;

    @Column(name = "FINE_SIZE")
    private Integer fineSize;

    @Column(name = "LOCATION")
    private String location;


    public Fine() {
    }


    public Integer getFineId() {
        return fineId;
    }

    public String getIsActive() {
        return isActive;
    }

    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(String expirationDate) {
        this.expirationDate = expirationDate;
    }

    public Integer getFineSize() {
        return fineSize;
    }

    public void setFineSize(Integer fineSize) {
        this.fineSize = fineSize;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    @Override
    public String toString() {
        return "Fine{" +
                "fineId=" + fineId +
                ", fineStatus='" + isActive + '\'' +
                ", startDate=" + startDate +
                ", expirationDate=" + expirationDate +
                ", fineSize=" + fineSize +
                ", location='" + location + '\'' +
                '}';
    }
}
