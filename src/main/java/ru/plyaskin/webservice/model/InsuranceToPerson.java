package ru.plyaskin.webservice.model;

import javax.persistence.*;

@Entity
@Table(name = "PERSON2INSURANCE")
public class InsuranceToPerson {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "GID")
    private Integer id;

    @Column(name = "INSURANCE_ID")
    private Integer insuranceId;

    @Column(name = "PERSON_ID")
    private Integer personId;


    public InsuranceToPerson() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getPersonId() {
        return personId;
    }

    public void setPersonId(Integer personId) {
        this.personId = personId;
    }

    public Integer getInsuranceId() {
        return insuranceId;
    }

    public void setInsuranceId(Integer insuranceId) {
        this.insuranceId = insuranceId;
    }

    @Override
    public String toString() {
        return "PersonToInsurance{" +
                "id=" + id +
                ", personId=" + personId +
                ", insuranceId=" + insuranceId +
                '}';
    }
}
