package ru.plyaskin.webservice.service;

import com.github.javafaker.Faker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.plyaskin.webservice.model.Car;
import ru.plyaskin.webservice.model.CarToFine;
import ru.plyaskin.webservice.model.Fine;
import ru.plyaskin.webservice.model.Person;
import ru.plyaskin.webservice.repositories.Car2FineRepo;
import ru.plyaskin.webservice.repositories.CarRepo;
import ru.plyaskin.webservice.repositories.FineRepo;
import ru.plyaskin.webservice.repositories.PersonRepo;

import java.text.SimpleDateFormat;
import java.util.Random;

@Service
public class FakeInfoCreationServiceImpl implements FakeInfoCreationService{

    @Autowired
    private PersonRepo personRepo;
    @Autowired
    private CarRepo carRepo;
    @Autowired
    private FineRepo fineRepo;
    @Autowired
    private Car2FineRepo car2FineRepo;
    private final Faker faker = new Faker();
    private final String[] carModel = new String[]{"Kia","Mitsubishi","Honda","Reno","Volkswagen"};
    private static final String numbers = "0123456789";
    private static final String letters = "АВЕКМНОРСТУХ";
    private final Random randomInt = new Random(System.currentTimeMillis());
    private final SimpleDateFormat df = new SimpleDateFormat("YYYY-MM-dd");

    /*

     */

    @Override
    public void generateFakeInfo() {
        for(int i = 0; i < 5; i++){
            Person person = new Person();
            person.setFirstName(faker.name().firstName());
            person.setLastName(faker.name().lastName());

            Car car = new Car();
            car.setCarModel(carModel[randomInt.nextInt(4)]);
            car.setLicensePlate(generateServicePlate());
            car.setYearOfCarManufacture(df.format(faker.date().birthday()));
            car.setCarOwner(person);
            carRepo.save(car);

            Fine fine = new Fine();
            fine.setIsActive("Y");
            fine.setFineSize(500);
            fine.setLocation(faker.address().fullAddress());
            fine.setExpirationDate(df.format(faker.date().birthday()));
            fine.setStartDate(df.format(faker.date().birthday()));

            fineRepo.save(fine);

            CarToFine carToFine = new CarToFine();
            carToFine.setCarId(car);
            carToFine.setFineId(fine);

            car2FineRepo.save(carToFine);
        }
    }



    public String generateServicePlate() {
        StringBuilder licensePlateBuilder = new StringBuilder();
        licensePlateBuilder.append(letters.charAt(randomInt.nextInt(11)));
        for(int i = 0; i < 3; i++){
            licensePlateBuilder.append(numbers.charAt(randomInt.nextInt(9)));
        }
        for(int i = 0; i < 2; i++){
            licensePlateBuilder.append(letters.charAt(randomInt.nextInt(11)));
        }
        return licensePlateBuilder.toString();
    }

    @Override
    public void cleanDatabase(){
        personRepo.deleteAll();
        carRepo.deleteAll();
    }

}
