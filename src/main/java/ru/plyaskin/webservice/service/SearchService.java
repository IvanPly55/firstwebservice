package ru.plyaskin.webservice.service;

import ru.plyaskin.webservice.model.FullInfo;
import ru.plyaskin.webservice.model.Person;

public interface SearchService {
    FullInfo findFullInfo(String licencePlate);
    Person findPersonByLicencePlate(String licencePlate);
    void requestValidation();

}
