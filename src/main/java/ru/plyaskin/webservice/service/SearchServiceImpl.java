package ru.plyaskin.webservice.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.plyaskin.webservice.model.FullInfo;
import ru.plyaskin.webservice.model.Person;
import ru.plyaskin.webservice.repositories.PersonRepo;

@Service
public class SearchServiceImpl implements SearchService {


    @Override
    public FullInfo findFullInfo(String licencePlate) {
        return null;
    }

    @Override
    public Person findPersonByLicencePlate(String licencePlate) {
        return null;
    }

    @Override
    public void requestValidation() {
    }

}
