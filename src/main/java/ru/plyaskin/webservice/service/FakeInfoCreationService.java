package ru.plyaskin.webservice.service;

public interface FakeInfoCreationService {
    void generateFakeInfo();
    void cleanDatabase();
}
