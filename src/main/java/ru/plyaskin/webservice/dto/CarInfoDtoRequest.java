package ru.plyaskin.webservice.dto;

public class CarInfoDtoRequest {
    private String licensePlate;

    public String getLicensePlate() {
        return licensePlate;
    }
}
